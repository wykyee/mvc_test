from sqlite3 import Connection


def create_users_table(conn: Connection):
    sql = """
    CREATE TABLE IF NOT EXISTS users (
        username VARCHAR(255),
        full_name VARCHAR(255)
    )
    """
    with conn:
        cursor = conn.cursor()
        cursor.execute(sql)
