import os.path
import pathlib
from sqlite3 import Connection, connect

from users.controllers.gui import UserGUIController
from users.controllers.cli import UserCLIController
from users.models import UserModel
from users.views.gui import UserGUIView
from users.views.cli import UserCLIView
from utils import create_users_table


CLI = "CLI"
GUI = "GUI"


def __run_cli(conn: Connection):
    model = UserModel(conn)
    view = UserCLIView()
    controller = UserCLIController(model, view)
    controller.start()


def __run_gui(conn: Connection):
    model = UserModel(conn)
    view = UserGUIView()
    controller = UserGUIController(model, view)

    view.set_controller(controller)
    controller.start()


run_method = {
    CLI: __run_cli,
    GUI: __run_gui,
}


def run(run_type: str) -> None:
    db_path = os.path.join(pathlib.Path(__file__).parent.resolve(), "users.db")
    try:
        with connect(db_path) as conn:
            create_users_table(conn)
            run_method[run_type](conn)
    except KeyError:
        print("Error while starting users: Unavailable run type")


if __name__ == "__main__":
    run(CLI)
    # run(GUI)
