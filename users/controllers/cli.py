from ..models import UserData


class UserCLIController:
    def __init__(self, model, view):
        self.model = model
        self.view = view
        self.users = list(self.model.read())
        self.choices = {
            "1": self.get_users,
            "2": self.create_user,
            "3": self.update_user,
            "4": self.delete_user,
        }

    def get_users(self):
        self.view.show_users_list(self.users)

    def create_user(self):
        username = input("Enter username: ")
        full_name = input("Enter full name: ")
        user_data = UserData(username=username, full_name=full_name)
        user = self.model.create(user_data)
        self.users.append(user)
        self.view.show_user_create_success(user)

    def update_user(self):
        try:
            rowid = int(input("Enter row id: "))
        except ValueError:
            self.view.show_get_by_rowid_error()
            return

        if rowid not in self.__get_all_rowids():
            self.view.show_get_by_rowid_error(rowid)
            return

        user: UserData = list(filter(lambda u: u.rowid == rowid, self.users))[0]
        list_index = self.users.index(user)
        username = input("Enter username: ")
        full_name = input("Enter full name: ")

        updated_user = UserData(username, full_name, rowid=user.rowid)
        self.model.update(updated_user)
        self.users[list_index] = updated_user
        self.view.show_user_update_success(user, updated_user)

    def delete_user(self):
        try:
            rowid = int(input("Enter row id: "))
        except ValueError:
            self.view.show_get_by_rowid_error()
            return

        if rowid not in self.__get_all_rowids():
            self.view.show_get_by_rowid_error(rowid)
            return

        user: UserData = list(filter(lambda u: u.rowid == rowid, self.users))[0]
        self.model.delete(user)
        self.users.remove(user)
        self.view.show_user_delete_success(user)

    def __get_all_rowids(self) -> list[int]:
        return [u.rowid for u in self.users]

    def start(self):
        while True:
            self.view.show_main_menu()
            choice = input("Enter choice\n")
            try:
                self.choices[choice]()
            except KeyError:
                self.view.show_exit_msg()
                break
