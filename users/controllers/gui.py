from ..views.gui import NewUser


class UserGUIController(object):
    def __init__(self, model, view):
        self.model = model
        self.view = view
        self.selection = None
        self.users = list(model.read())

    def create_user(self):
        new_user = NewUser(self.view).show()
        if new_user:
            user = self.model.create(new_user)
            self.users.append(user)
            self.view.add_user(user)

    def select_user(self, index):
        self.selection = index
        user = self.users[index]
        self.view.load_details(user)

    def update_user(self):
        if self.selection is None:
            return
        rowid = self.users[self.selection].rowid
        update_user = self.view.get_details()
        update_user.rowid = rowid

        user = self.model.update(update_user)
        self.users[self.selection] = user
        self.view.update_user(user, self.selection)

    def delete_user(self):
        if self.selection is None:
            return
        user = self.users[self.selection]
        self.model.delete(user)
        self.view.remove_user(self.selection)

    def start(self):
        for u in self.users:
            self.view.add_user(u)
        self.view.mainloop()
