class UserData:
    def __init__(self, username: str, full_name: str, rowid=None):
        self.username = username
        self.full_name = full_name
        self.rowid = rowid

    def __repr__(self):
        rowid = self.rowid if self.rowid else ""
        return f"User(username={self.username},{rowid=})"


class UserModel:
    def __init__(self, conn):
        self.conn = conn

    @staticmethod
    def to_values(user: UserData) -> tuple:
        return user.username, user.full_name

    def create(self, user: UserData):
        sql = "INSERT INTO users VALUES (?, ?)"
        with self.conn:
            cursor = self.conn.cursor()
            cursor.execute(sql, self.to_values(user))
            user.rowid = cursor.lastrowid
        return user

    def read(self):
        sql = "SELECT rowid, username, full_name FROM users"
        for row in self.conn.execute(sql):
            user = UserData(*row[1:])
            user.rowid = row[0]
            yield user

    def update(self, user: UserData):
        sql = "UPDATE users SET username = ?, full_name = ? WHERE rowid = ?"
        with self.conn:
            self.conn.execute(sql, self.to_values(user) + (user.rowid,))
        return user

    def delete(self, user: UserData):
        sql = "DELETE FROM users WHERE rowid = ?"
        with self.conn:
            self.conn.execute(sql, (user.rowid,))

