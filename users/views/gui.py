import tkinter as tk

from ..models import UserData


class UserList(tk.Frame):
    def __init__(self, master, **kwargs):
        super().__init__(master)
        self.lb = tk.Listbox(self, **kwargs)
        scroll = tk.Scrollbar(self, command=self.lb.yview)

        self.lb.config(yscrollcommand=scroll.set)
        scroll.pack(side=tk.RIGHT, fill=tk.Y)
        self.lb.pack(side=tk.LEFT, fill=tk.BOTH, expand=1)

    def insert(self, user: UserData, index: str = tk.END):
        self.lb.insert(index, f"{user.username}, {user.full_name}")

    def delete(self, index):
        self.lb.delete(index, index)

    def update_instance(self, user: UserData, index):
        self.delete(index)
        self.insert(user, index)

    def bind_double_click(self, callback):
        self.lb.bind("<Double-Button-1>", lambda _: callback(self.lb.curselection()[0]))


class UserForm(tk.LabelFrame):
    fields = ("Username", "Полное имя",)

    def __init__(self, master, **kwargs):
        super().__init__(master, text="User", padx=10, pady=10, **kwargs)
        self.frame = tk.Frame(self)
        self.entries = list(map(self.create_field, enumerate(self.fields)))
        self.frame.pack()

    def create_field(self, field):
        position, text = field
        label = tk.Label(self.frame, text=text)
        entry = tk.Entry(self.frame, width=25)
        label.grid(row=position, column=0, pady=5)
        entry.grid(row=position, column=1, pady=5)
        return entry

    def load_details(self, user: UserData):
        values = (user.username, user.full_name,)
        for entry, value in zip(self.entries, values):
            entry.delete(0, tk.END)
            entry.insert(0, value)

    def get_details(self):
        values = [e.get() for e in self.entries]
        return UserData(*values)

    def clear(self):
        for entry in self.entries:
            entry.delete(0, tk.END)


class NewUser(tk.Toplevel):
    def __init__(self, parent):
        super().__init__(parent)
        self.contact = None
        self.form = UserForm(self)
        self.btn_add = tk.Button(self, text="Добавить", command=self.confirm)
        self.form.pack(padx=10, pady=10)
        self.btn_add.pack(pady=10)

    def confirm(self):
        self.contact = self.form.get_details()
        if self.contact:
            self.destroy()

    def show(self):
        self.grab_set()
        self.wait_window()
        return self.contact


class UpdateUserForm(UserForm):
    def __init__(self, master, **kwargs):
        super().__init__(master, **kwargs)
        self.btn_save = tk.Button(self, text="Сохранить")
        self.btn_delete = tk.Button(self, text="Удалить")

        self.btn_save.pack(side=tk.RIGHT, ipadx=5, padx=5, pady=5)
        self.btn_delete.pack(side=tk.RIGHT, ipadx=5, padx=5, pady=5)

    def bind_save(self, callback):
        self.btn_save.config(command=callback)

    def bind_delete(self, callback):
        self.btn_delete.config(command=callback)


class UserGUIView(tk.Tk):
    def __init__(self):
        super().__init__()
        self.title("Пользователи")
        self.list = UserList(self, height=15)
        self.form = UpdateUserForm(self)
        self.btn_new = tk.Button(self, text="Добавить нового пользователя")

        self.list.pack(side=tk.LEFT, padx=10, pady=10)
        self.form.pack(padx=10, pady=10)
        self.btn_new.pack(side=tk.BOTTOM, pady=5)

    def set_controller(self, controller):
        self.btn_new.config(command=controller.create_user)
        self.list.bind_double_click(controller.select_user)
        self.form.bind_save(controller.update_user)
        self.form.bind_delete(controller.delete_user)

    def add_user(self, user: UserData):
        self.list.insert(user)

    def update_user(self, user: UserData, index):
        self.list.update_instance(user, index)

    def remove_user(self, index):
        self.form.clear()
        self.list.delete(index)

    def get_details(self):
        return self.form.get_details()

    def load_details(self, user: UserData):
        self.form.load_details(user)

