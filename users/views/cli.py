from ..models import UserData


class UserCLIView:
    @staticmethod
    def show_main_menu():
        print("\nMenu:\n1 - read\n2 - create\n3 - update\n4 - delete\n0 - exit\n")

    @staticmethod
    def show_users_list(users: list[UserData]):
        print("\n------ USERS LIST -------")
        for user in users:
            print(f"({user.rowid}) Username: {user.username}\n\tFull name: {user.full_name}")

    @staticmethod
    def show_user_create_success(user: UserData):
        print(f"\nUser has been added\nUsername: {user.username}, Full name: {user.full_name}")

    @staticmethod
    def show_user_update_success(old_user: UserData, user: UserData):
        print(f"User has changed:\nUsername: {old_user.username} -> {user.username}, "
              f"Full name: {old_user.full_name} -> {user.full_name}")

    @staticmethod
    def show_get_by_rowid_error(rowid=None):
        print(f"Row {rowid if rowid else ''} doesn't exist")

    @staticmethod
    def show_user_delete_success(user: UserData):
        print(f"User has been deleted:\nUsername: {user.username}, Full name: {user.full_name}")

    @staticmethod
    def show_exit_msg():
        print("Goodbye!")
